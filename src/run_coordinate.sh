#!/bin/csh
# 1-5 player coordination game with 1,2 or 3 goals
foreach goals (1 2 3)
 foreach agents (2 3 4 5)
   set outfile = coord_p${agents}g${goals}.out
    if (-e $outfile) then
     echo "Output file $outfile already exists."
     exit
    endif
 end
end
foreach goals (1 2 3)
 foreach agents (2 3 4 5)
   set outfile = coord_p${agents}g${goals}.out
   echo "Working on $goals goals and $agents agents. Output file: $outfile"
   ./coordinate.sh $goals $agents > $outfile
 end
end
