package org.planetsolace.jenvirocon;

import java.util.*;
import java.io.*;

class OneGoalCoordinationGameFactoryTest {

    static int verbose = 1;

    static void run(int agents) {
	OneGoalCoordinationGameFactory factory = new OneGoalCoordinationGameFactory(agents);
	Game game;
	GameScorer scorer = new AverageGoalScorer();
	List<GameScorer> scorers = new ArrayList<GameScorer>();
	scorers.add(scorer);
	scorers.add(new AllGoalScorer());
	// scorers.add(new BlackSheepScorer());
	double[][] thresholds = new double[2][1];
	thresholds[0][0] = agents-1.0;
	thresholds[1][0] = agents+1.0;
	scorers.add(new VariableLoadScorer(thresholds));
	while ((game = factory.createNext()) != null) {
	    int goals = game.getGoalCount();
	    double motSum = game.computeMotivationSum();
	    double motSqSum = game.computeMotivationSquareSum();
	    double ex = motSum / (agents * goals); // expected motivation per goal per agent
	    double ex2 = motSqSum / (agents * goals); // expected square of motivation per goal per agent
	    double variance = ex2 - (ex * ex);
	    double polarization = game.computeMeanPolarization();
	    System.out.print("" + agents + " " + goals + " " + game.getName() + " " + motSum + " " + (motSum/(agents * goals)) + " " + variance + " " + polarization);
	    for (int j = 0; j < scorers.size(); ++j) { // different scores
		System.out.print(" " + scorers.get(j).getName() + " " + scorers.get(j).computeScore(game));
	    }
	    System.out.println();
	    if (verbose > 1) {
		System.out.println("#### DETAILS #####");
		List<ArrayList<Integer> > equilibria = game.findNashEquilibria();
		game.printEquilibria(System.out, equilibria);
	    }
	}
    }

    public static void main(String[] args) {
	int agents = 4;
	int goalCount = 2;
	if (args.length > 0) {
	    try {
		agents = Integer.parseInt(args[0]);
	    } catch (NumberFormatException nfe) {
		System.out.println("Error: cannot parse command line paramenter as integer value!");
		System.exit(1);
	    }
	}
	run(agents);
    }

}