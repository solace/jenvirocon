package org.planetsolace.jenvirocon;

import java.util.*;
import java.io.*;

class MultiMatrix {

    int[] dims;
    double[] values;

    
    MultiMatrix(int[] _dims) {
	dims = new int[_dims.length];
	int mul = 1;
	for (int i = 0; i < dims.length; ++i) {
	    dims[i] = _dims[i];
	    mul *= dims[i];
	}
	values = new double[mul]; 
    }

    int[] getDims() { return dims; }

    int getIndex(int[] coords) {
	int id = 0;
	int mul = 1;
	for (int i = 0; i < coords.length; ++i) {
	    id += mul*coords[i];
	    mul *= dims[i];
	}
	assert (id < size());
	return id;
    }

    /** Speedup: coordinates are concatenation of coord0 and coords */
    int getIndex(int coord0, int[] coords) {
	int id = 0;
	int mul = 1;
	id += coord0;
	mul *= dims[0];
	for (int i = 0; i < coords.length; ++i) {
	    id += mul*coords[i];
	    mul *= dims[i+1];
	}
	if (id >= size()) {
	    System.out.println("" + coord0);
	    for (int i = 0; i < coords.length; ++i) {
		System.out.println("" + i + " : "  + coords[i]);
	    }
	}
	assert (id < size());
	return id;
    }

    double getValue(int[] coords) {
	return values[getIndex(coords)];

    }
    double getValue(int coord0, int[] coords) {
	return values[getIndex(coord0, coords)];
    }

    void setValue(int[] coords, double value) {
	assert getIndex(coords) < values.length;
	values[getIndex(coords)] = value;
    }

    void setValue(int coord0, int[] coords, double value) {
	values[getIndex(coord0, coords)] = value;
    }

    int size() { 
	return values.length;
    }

    public String toString() {
	StringBuffer buf = new StringBuffer();
	for (MultiIterator it = new MultiIterator(dims); !it.isStartedOver(); it.inc()) {
	    int[] digits = it.getDigits();
	    for (int j = 0; j < digits.length; ++j) {
		buf.append("" + digits[j] + " ");
	    }
	    buf.append(": ");
	    buf.append("" + getValue(digits) + "\n");
	}
	return buf.toString();
    }

}