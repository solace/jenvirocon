package org.planetsolace.jenvirocon;

import java.util.*;

interface GameFactory {

    List<Game> create();

    /** Creates a game for a given (implementation-dependent) name indicating details of the game setup. */
    Game createGame(String name); 

    /** Creates next game. Returns null if no next game can be created. */
    Game createNext();

    /** Returns the character that represents a player who has a low motivations towards all goals. */
    char getDefaultChar();

}