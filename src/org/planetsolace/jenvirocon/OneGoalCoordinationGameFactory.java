package org.planetsolace.jenvirocon;

import java.util.*;

class OneGoalCoordinationGameFactory implements GameFactory {

    int agents = 4;
    double[] thresholds = { 4.0 };
    String[] choices = {"d", "c", "h"};
    double[] costs = {0.0, 1.0, 2.0};
    double motivationDelta = 0.25;
    public static final int GOAL_COUNT = 1;
    private static final int goalCount = GOAL_COUNT;
    int[] dims;
    DigitChecker checker = new SortedChecker(); // only "sorted" populations to avoid overcounting boring permutations
    MultiIterator it;

    public static final int D = 0;
    public static final int C = 1;
    public static final int H = 2;
    public static final int KINDS = 3;
    public static final String KIND_CHARS = "dch"; 

    public OneGoalCoordinationGameFactory(int _agents) {
	agents = _agents;
	dims = new int[agents];
	for (int i = 0; i < dims.length; ++i) {
	    dims[i] = KINDS;
	}
	it = new MultiIterator(dims);
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	thresholds[0] = (double)agents;
	// now heroic efforts are simply twice the cooperation effort
	// costs[costs.length-1] = thresholds[0]; // super-heroic effort
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    private Game createGame(int[] kinds) {
	assert (kinds.length == agents);
	double[][] motivations = new double[agents][goalCount];
	double[] aversions = new double[agents];
	String name = new String(); // Builder("XXXX");
	for (int i = 0; i < kinds.length; ++i) { // loop over agents
	    motivations[i][0] = costs[kinds[i]] + motivationDelta; // i'th player is motivated to solve the one goal
	    String c = KIND_CHARS.substring(kinds[i], kinds[i]+1);
	    assert c.length() == 1;
	    name = name + c;
	}
	Game game = new Game(agents, aversions,  motivations, choices, costs, thresholds);
	game.setName(name.toString());
	return game;
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    public Game createGame(String population) {
	assert (population.length() == agents);
	int[] kinds = new int[population.length()];
	for (int i = 0; i < population.length(); ++i) {
	    char c = population.charAt(i);
	    // find in existing string
	    boolean found = false;
	    for (int j = 0; j < KIND_CHARS.length(); ++j) {
		char c2 = KIND_CHARS.charAt(j);
		if (c == c2) {
		    found = true;
		    kinds[i] = j;
		    break;
		}
	    }
	    assert found; // otherwise unknown kind of population member
	}
	return createGame(kinds);
    }

    /** Creates a list of games */
    public List<Game> create() {
	it.reset();
	ArrayList<Game> result = new ArrayList<Game>();
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	for (; !it.isStartedOver(); it.inc()) {
	    Game game = createGame(it.getDigits());
	    result.add(game);
	}
	return result;
    }

    /** Creates next game. Returns null if done. */
    public Game createNext() {
	if (it.isStartedOver()) {
	    it.inc();
	    return null;
	}
	Game game = createGame(it.getDigits());
	it.inc();
	return game;
    }

    public char getDefaultChar() { return KIND_CHARS.charAt(D); }

    void setAgents(int _agents) {
	agents = _agents;
    }



}