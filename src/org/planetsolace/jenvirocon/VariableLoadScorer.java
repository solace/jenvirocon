package org.planetsolace.jenvirocon;

import java.util.*;

class VariableLoadScorer implements GameScorer {

    String name = "VL";

    AverageGoalScorer scorer = new AverageGoalScorer();

    double defaultMotivation = 0.0;

    double[][] alternativeThresholds;

    public VariableLoadScorer(double[][] _alternativeThresholds) {
	alternativeThresholds = _alternativeThresholds;
    }

    public double computeScore(Game game) {
	assert alternativeThresholds != null;
	assert game.getGoalCount() == alternativeThresholds[0].length;
	double scoreSum = 0.0;
	for (int i = 0; i < alternativeThresholds.length; ++i) {
	    Game modGame = new Game(game.getPlayerCount(), game.getInequalityAversions(), game.getMotivations(), game.getChoiceNames(), game.getCosts(), alternativeThresholds[i]);
	    double score = scorer.computeScore(modGame);
	    // System.out.println("VarLoad sub-Score " + (i+1) + " : " 
	    // + score);
	    scoreSum += score;
	}
	scoreSum /= alternativeThresholds.length;
	return scoreSum;
    }

    public String getName() { return name; }

    public void setName(String _name) { name = _name; }

}