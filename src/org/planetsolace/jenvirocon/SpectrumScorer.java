package org.planetsolace.jenvirocon;

import java.util.*;

/** Computes maximum difference between any two players in goal priorities (that is the maximal squared Euclidian norm between motivation vectors)*/
class SpectrumScorer implements GameScorer {

    /** Computes score of a game. In this case it scores the motivations, not the outcome of the game. */
    public double computeScore(Game game) {
	int goalCount = game.getGoalCount();
	if (goalCount < 2) {
	    return 0.0;
	}
	int playerCount = game.getPlayerCount();
	double[][] motivations = game.getMotivations();
	double distMax = 0.0;
	for (int i = 0; i < playerCount; ++i) {
	    for (int j = i+1; j < playerCount; ++j) {
		double dist = 0.0;
		double d;
		for (int k = 0; k < goalCount; ++k) {
		    d = motivations[i][k] - motivations[j][k];
		    dist += d * d; // euclidian norm squared
		}
		if (dist > distMax) {
		    distMax = dist;
		}
	    }
	}
	return distMax;
    }

    public String getName() { return new String("Spectrum"); }

}