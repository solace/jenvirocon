package org.planetsolace.jenvirocon;

import java.util.*;
import java.io.*;

class GameFactoryIQARunner implements Runnable {

    GameFactory factory;

    List<GameScorer> scorers;

    int goalCount;

    double centristThreshold = 0.25;

    int iterMax = 100000; // not more than this many iterations

    double threshVariation = 1.0;

    double[][] variableLoadThresholds;

    static int verbose = 1;

    public GameFactoryIQARunner() { }

    /** Returns set of variable thresholds in which two goals are varied.
     * For n goals, returns 2*n goal threshold vectors. */
// 	thresholds[0][1] = threshPerGoal + threshVariation;
// 	thresholds[1][0] = threshPerGoal + threshVariation;
// 	thresholds[1][1] = threshPerGoal - threshVariation;
    static double[][] createThresholds(int goalCount, double threshPerGoal, double threshVariation) {
	ArrayList<ArrayList<Double> > result = new ArrayList<ArrayList<Double> >();
	for (int i = 0; i < goalCount; ++i) {
	    ArrayList<Double> thresholds = new ArrayList<Double>();
	    for (int k = 0; k < goalCount; ++k) {
		thresholds.add(threshPerGoal);
	    }
	    assert thresholds.size() == goalCount;
	    thresholds.set(i, threshPerGoal - threshVariation); // i'th goal is a bit easier
	    result.add(thresholds);
	}
	for (int i = 0; i < goalCount; ++i) {
	    ArrayList<Double> thresholds = new ArrayList<Double>();
	    for (int k = 0; k < goalCount; ++k) {
		thresholds.add(threshPerGoal);
	    }
	    assert thresholds.size() == goalCount;
	    thresholds.set(i, threshPerGoal + threshVariation); // i'th goal is a bit harder
	    result.add(thresholds);
	}
	assert(result.get(0).size() == goalCount);
	double[][] finalResult = new double[result.size()][goalCount];
	for (int i = 0; i < result.size(); ++i) {
	    for (int j = 0; j < result.get(i).size(); ++j) {
		finalResult[i][j] = result.get(i).get(j);
	    }
	}
	return finalResult;
    }

    public void run() {
	assert(factory != null);
	// CoordinationGameFactoryAll factory = new CoordinationGameFactoryAll(agents);
	Game game;
	System.out.print("\"Agents\" \"Goals\" \"Motivations\" \"MotivationSum\" \"MeanMotivation\" \"Variance\" \"Polarization\" \"TIQA\"");
	for (int j = 0; j < scorers.size(); ++j) { // different scores
	    System.out.print(" \"" + scorers.get(j).getName() + "\"");
	}
	System.out.println();
	int iterations = 0;
	while (((game = factory.createNext()) != null) && (iterations++ < iterMax))  {
	    int goals = game.getGoalCount();
	    int agents = game.getPlayerCount();
	    double motSum = game.computeMotivationSum();
	    double[] aversions = game.getInequalityAversions();
	    double tiqa = 0.0; // total inequality aversion
	    for (double a : aversions) { // sum of individual inequality aversions
		tiqa += a;
	    }
	    double motSqSum = game.computeMotivationSquareSum();
	    double ex = motSum / (agents * goals);
	    double ex2 = motSqSum / (agents * goals);
	    double variance = ex2 - (ex * ex);
	    double polarization = game.computeMeanPolarization();
	    System.out.print("" + agents + " " + goals + " " + game.getName() + " " + motSum + " " + (motSum/(agents * goals)) + " " + variance + " " + polarization + " " + tiqa);
	    for (int j = 0; j < scorers.size(); ++j) { // different scores
		System.out.print(" " + scorers.get(j).computeScore(game));
	    }
	    System.out.println();
	    if (verbose > 1) {
		System.out.println("#### DETAILS #####");
		System.out.println(game.toString());
		List<ArrayList<Integer> > equilibria = game.findNashEquilibria();
		game.printEquilibria(System.out, equilibria);
	    }
	}
    }

    void init(int goalCount, int agents, String mode) {
	assert(goalCount > 0);
	assert(goalCount <= 3);
	double totalThresh = (double)agents;
	double threshPerGoal = totalThresh / goalCount;
	variableLoadThresholds = createThresholds(goalCount, threshPerGoal, threshVariation);
	// if (goalCount == 1) {
	    // factory = new OneGoalCoordinationGameFactory(agents);
	// } else 
	if (goalCount == 2) {
	    factory = new CoordinationGameIQAFactoryAll(agents);
	} else {
	    System.out.println("Currently only two-goal games are implemented for inequality aversion.");
	    assert(false); // should never happen
	    System.exit(1);
	}

	// else if (goalCount == 3) {
	// ThreeGoalCoordinationGameFactory f3 = new ThreeGoalCoordinationGameFactory(agents);
	// if (mode.equals("r") ) {
	// System.out.println("# Activating random mode.");
	// f3.setRandomMode(true);
	// }
	// factory = f3;
	GameScorer scorer = new AverageGoalScorer();
	scorers = new ArrayList<GameScorer>();
	scorers.add(scorer);
	scorers.add(new AllGoalScorer());
	scorers.add(new BlackSheepScorer(factory));
	scorers.add(new VariableLoadScorer(variableLoadThresholds));
	scorers.add(new SpectrumScorer());
	scorers.add(new BiasScorer());
	scorers.add(new CentristScorer(centristThreshold));
    }

    /** Prints error message and quits program. */
    static void fatalError(String message) {
	System.err.println(message);
	System.exit(1);
    }

    public static void main(String[] args) {
	int agents = 0;
	int goalCount = 0;
	String mode = "";
	if (args.length > 1) {
	    try {
		goalCount = Integer.parseInt(args[0]);
		agents = Integer.parseInt(args[1]);
	    } catch (NumberFormatException nfe) {
		System.out.println("Error: cannot parse command line paramenter as integer value!");
		System.exit(1);
	    }
	    if (args.length > 2) {
		mode = args[2];
	    }
	} else {
	    fatalError("Usage: specify the number of goals and the number of agents in the command line.");
	}
	if (goalCount != 2) {
	    fatalError("The number of goals has to be 2.");
	}
	GameFactoryIQARunner runner = new GameFactoryIQARunner();
	runner.init(goalCount, agents, mode);
	runner.run();
    }

}