package org.planetsolace.jenvirocon;

import java.util.*;

class ThreeGoalCoordinationGameFactory implements GameFactory {

    int agents = 4;
    int goalCount = 3;
    String[] choices = {"d", "c", "h"};
    double[] costs = {0.0, 0.3333334, 1.0};
    double[] thresholds = { 4.0/3.0, 4.0/3.0, 4.0/3.0 };
    double motivationDelta = 0.25; // important that this inumber is smaller than 1/goalCount
    int[] dims;
    DigitChecker checker = new SortedChecker(); // only "sorted" populations to avoid overcounting boring permutations
    MultiIterator it;
    boolean randomMode = false;

    public static final int D = 0;
    public static final int C = 1;
    public static final int H = 2; // CAREFUL: numbers change in other classes (H=2,C=1,D=0)

    public static final int DDD = 0;  // a 
    public static final int CDD = 1;  // b
    public static final int HDD = 2;  // c
    public static final int DCD = 3;  // d
    public static final int CCD = 4;  // e
    public static final int HCD = 5;  // f
    public static final int DHD = 6;  // g
    public static final int CHD = 7;  // h
    public static final int HHD = 8;  // i

    public static final int DDC = 9; // j
    public static final int CDC = 10; // k
    public static final int HDC = 11; // l
    public static final int DCC = 12; // m
    public static final int CCC = 13; // n
    public static final int HCC = 14; // o
    public static final int DHC = 15; // p
    public static final int CHC = 16; // q
    public static final int HHC = 17; // r

    public static final int DDH = 18; // s
    public static final int CDH = 19; // t
    public static final int HDH = 20; // u
    public static final int DCH = 21; // v
    public static final int CCH = 22; // w
    public static final int HCH = 23; // x
    public static final int DHH = 24; // y
    public static final int CHH = 25; // z
    public static final int HHH = 26; // H

    public static final int KINDS = 27;

    public static final String KIND_CHARS = "abcdefghijklmnopqrtsuvwxyzH"; // left, center, right characters in one string


    public ThreeGoalCoordinationGameFactory(int _agents) {
	agents = _agents;
	dims = new int[agents];
	for (int i = 0; i < dims.length; ++i) {
	    dims[i] = KINDS;
	}
	it = new MultiIterator(dims);
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	double totThreshold = (double)agents;
	double thresholdPerGoal = totThreshold / goalCount;
	for (int i = 0; i < thresholds.length; ++i) {
	    thresholds[i] = thresholdPerGoal;
	}
	// costs[costs.length-1] = thresholdPerGoal;
	costs[costs.length-1] = 1; // all efforts towards one goal
	costs[1] = 1.0/(double)goalCount; // efforts spread out between goals
    }

    double[] createAgentMotivation(int kind) {
	double[] result = new double[goalCount];
	switch (kind) {
	case DDD: // = 0;  // a 
	    result[0] = costs[D];
	    result[1] = costs[D];
	    result[2] = costs[D];
	    break;
	case CDD://  = 1;  // b
	    result[0] = costs[C];
	    result[1] = costs[D];
	    result[2] = costs[D];
	    break;
	case HDD: //=  2;  // c
	    result[0] = costs[H];
	    result[1] = costs[D];
	    result[2] = costs[D];
	    break;
	case DCD: // =  3;  // d
	    result[0] = costs[D];
	    result[1] = costs[C];
	    result[2] = costs[D];
	    break;
	case CCD: // =  4;  // e
	    result[0] = costs[C];
	    result[1] = costs[C];
	    result[2] = costs[D];
	    break;
	case HCD: // =  5;  // f
	    result[0] = costs[H];
	    result[1] = costs[C];
	    result[2] = costs[D];
	    break;
	case DHD: // =  6;  // g
	    result[0] = costs[D];
	    result[1] = costs[H];
	    result[2] = costs[D];
	    break;
	case CHD: // =  7;  // h
	    result[0] = costs[C];
	    result[1] = costs[H];
	    result[2] = costs[D];
	    break;
	case HHD: // =  8;  // i
	    result[0] = costs[H];
	    result[1] = costs[H];
	    result[2] = costs[D];
	    break;
	    
	case DDC: // =  9; // j
	    result[0] = costs[D];
	    result[1] = costs[D];
	    result[2] = costs[C];
	    break;
	case CDC: // =  10; // k
	    result[0] = costs[C];
	    result[1] = costs[D];
	    result[2] = costs[C];
	    break;
	case HDC: // =  11; // l
	    result[0] = costs[H];
	    result[1] = costs[D];
	    result[2] = costs[C];
	    break;
	case DCC: // =  12; // m
	    result[0] = costs[D];
	    result[1] = costs[C];
	    result[2] = costs[C];
	    break;
	case CCC: // =  13; // n
	    result[0] = costs[C];
	    result[1] = costs[C];
	    result[2] = costs[C];
	    break;
	case HCC: // =  14; // o
	    result[0] = costs[H];
	    result[1] = costs[C];
	    result[2] = costs[C];
	    break;
	case DHC: // =  15; // p
	    result[0] = costs[D];
	    result[1] = costs[H];
	    result[2] = costs[C];
	    break;
	case CHC: // =  7; // q
	    result[0] = costs[C];
	    result[1] = costs[H];
	    result[2] = costs[C];
	    break;
	case HHC: // =  8; // r
	    result[0] = costs[H];
	    result[1] = costs[H];
	    result[2] = costs[C];
	    break;
	case DDH: // =  0; // s
	    result[0] = costs[D];
	    result[1] = costs[D];
	    result[2] = costs[H];
	    break;
	case CDH: // =  1; // t
	    result[0] = costs[C];
	    result[1] = costs[D];
	    result[2] = costs[H];
	    break;
	case HDH: // =  2; // u
	    result[0] = costs[H];
	    result[1] = costs[D];
	    result[2] = costs[H];
	    break;
	case DCH: // =  3; // v
	    result[0] = costs[D];
	    result[1] = costs[C];
	    result[2] = costs[H];
	    break;
	case CCH: // =  4; // w
	    result[0] = costs[C];
	    result[1] = costs[C];
	    result[2] = costs[H];
	    break;
	case HCH: // =  5; // x
	    result[0] = costs[H];
	    result[1] = costs[C];
	    result[2] = costs[H];
	    break;
	case DHH: // =  6; // y
	    result[0] = costs[D];
	    result[1] = costs[H];
	    result[2] = costs[H];
	    break;
	case CHH: // =  7; // z
	    result[0] = costs[C];
	    result[1] = costs[H];
	    result[2] = costs[H];
	    break;
	case HHH: // =  8; // H
	    result[0] = costs[H];
	    result[1] = costs[H];
	    result[2] = costs[H];
	    break;
	default:
	    System.out.println("Unknown kind of agent!");
	    System.exit(1);
	}
	for (int i =0; i < result.length; ++i) {
	    result[i] += motivationDelta;
	}
	return result;
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    private Game createGame(int[] kinds) {
	assert (kinds.length == agents);
	double[][] motivations = new double[agents][goalCount];
	double [] aversions = new double[agents];
	String name = new String(); // Builder("XXXX");
	for (int i = 0; i < kinds.length; ++i) { // loop over agents
	    double[] mots = createAgentMotivation(kinds[i]);
	    for (int j = 0; j < mots.length; ++j) {
		motivations[i][j] = mots[j]; // i'th player is motivated to solve goal j
	    }
	    String c = KIND_CHARS.substring(kinds[i], kinds[i]+1);
	    assert c.length() == 1;
	    name = name + c;
	}
	// double[] thresholds = {1.0, 1.0, 1.0}; //  total cost to ALL goals: is equal to number of players 
	// double totalThreshold = (double)agents; 
	// double threshPerGoal = totalThreshold / goalCount;
	// for (int i = 0; i < thresholds.length; ++i) {
	// thresholds[i] = threshPerGoal;
	// }
	Game game = new Game(agents, aversions, motivations, choices, costs, thresholds);
	game.setName(name.toString());
	return game;
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    public Game createGame(String population) {
	assert (population.length() == agents);
	int[] kinds = new int[population.length()];
	for (int i = 0; i < population.length(); ++i) {
	    char c = population.charAt(i);
	    // find in existing string
	    boolean found = false;
	    for (int j = 0; j < KIND_CHARS.length(); ++j) {
		char c2 = KIND_CHARS.charAt(j);
		if (c == c2) {
		    found = true;
		    kinds[i] = j;
		    break;
		}
	    }
	    assert found; // otherwise unknown kind of population member
	}
	return createGame(kinds);
    }

    /** Creates a list of games */
    public List<Game> create() {
	it.reset();
	ArrayList<Game> result = new ArrayList<Game>();
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	for (; !it.isStartedOver(); it.inc()) {
	    Game game = createGame(it.getDigits());
	    result.add(game);
	}
	return result;
    }

    /** Creates next game. Returns null if done. */
    public Game createNext() {
	Game game;
	if (randomMode) {
	    game = createGame(it.createRandom());
	} else {
	    if (it.isStartedOver()) {
		it.inc();
		return null;
	    }
	    game = createGame(it.getDigits());
	}
	it.inc();
	return game;
    }

    /** Returns character corresponding to member with low motivation towards all goals. */
    public char getDefaultChar() { return KIND_CHARS.charAt(DDD); }

    void setAgents(int _agents) {
	agents = _agents;
    }

    void setRandomMode(boolean mode) { randomMode = mode; }

}