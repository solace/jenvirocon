package org.planetsolace.jenvirocon;

import java.util.*;

class CoordinationGameFactoryAll implements GameFactory {

    int goalCount = 2;
    int agents = 4;

    double[] thresholds = {2.0, 2.0}; //  total cost to ALL goals is equal to number of players 
    String[] choices = {"d", "c", "h"};
    double[] costs = {0.0, 0.5, 1.0}; // choice for each goal: commit one, one half or no untits
    
    double motivationDelta = 0.25;

    int[] dims;
    DigitChecker checker = new SortedChecker(); // only "sorted" populations to avoid overcounting boring permutations
    MultiIterator it;

    public static final int D = 0;
    public static final int C = 1;
    // public static final int H = 2; // CAREFUL: numbers change in other classes (H=2,C=1,D=0)
    public static final int H = 2; // CAREFUL: numbers change in other classes (H=2,C=1,D=0)

    public static final int DD = 0; // d
    public static final int CD = 1; // e
    public static final int HD = 2; // l
    public static final int DC = 3; // f
    public static final int CC = 4; // c
    public static final int HC = 5; // L
    public static final int DH = 6; // r
    public static final int CH = 7; // R
    public static final int HH = 8; // B
    public static final int KINDS = 9;

    public static final String KIND_CHARS = "delfcLrRB"; // left, center, right characters in one string
    // public static final String KIND_CHARS = "dchDCHXYZ"; // left, center, right characters in one string

    public CoordinationGameFactoryAll(int _agents) {
	agents = _agents;
	dims = new int[agents];
	for (int i = 0; i < dims.length; ++i) {
	    dims[i] = KINDS;
	}
	it = new MultiIterator(dims);
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	double totalThreshold = (double)agents; 
	double threshPerGoal = totalThreshold / goalCount;
	for (int i = 0; i < thresholds.length; ++i) {
	    thresholds[i] = threshPerGoal;
	}
	// PREVIOUSLY:
	// costs[costs.length-1] = threshPerGoal; // super-heroic motivation
	// NOW:
	costs[costs.length-1] = 1;
	costs[1] = 1.0/(double)goalCount;
    }

    double[] createAgentMotivation(int kind) {
	double[] result = new double[goalCount];
	switch (kind) {
	case DD: 
	    result[0] = costs[D];
	    result[1] = costs[D];
	    break;
	case CD: 
	    result[0] = costs[C];
	    result[1] = costs[D];
	    break;
 	case HD: 
 	    result[0] = costs[H];
 	    result[1] = costs[D];
 	    break;
	case DC: 
	    result[0] = costs[D];
	    result[1] = costs[C];
	    break;
	case CC: 
	    result[0] = costs[C];
	    result[1] = costs[C];
	    break;
 	case HC: 
 	    result[0] = costs[H];
 	    result[1] = costs[C];
 	    break;
 	case DH: 
 	    result[0] = costs[D];
 	    result[1] = costs[H];
 	    break;
 	case CH: 
 	    result[0] = costs[C];
 	    result[1] = costs[H];
 	    break;
 	case HH: 
 	    result[0] = costs[H];
 	    result[1] = costs[H];
 	    break;
	default:
	    System.out.println("Unknown kind of agent!");
	    System.exit(1);
	}
	for (int i =0; i < result.length; ++i) {
	    result[i] += motivationDelta;
	}
	return result;
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    private Game createGame(int[] kinds) {
	assert (kinds.length == agents);
	double[][] motivations = new double[agents][goalCount];
	double[] aversions = new double[agents];
	String name = new String(); // Builder("XXXX");
	for (int i = 0; i < kinds.length; ++i) { // loop over agents
	    double[] mots = createAgentMotivation(kinds[i]);
	    for (int j = 0; j < mots.length; ++j) {
		motivations[i][j] = mots[j]; // i'th player is motivated to solve goal j
	    }
	    String c = KIND_CHARS.substring(kinds[i], kinds[i]+1);
	    assert c.length() == 1;
	    name = name + c;
	}
	Game game = new Game(agents, aversions, motivations, choices, costs, thresholds);
	game.setName(name.toString());
	return game;
    }

    /** Creates one game given a "population" of left, center and right-wing people */
    public Game createGame(String population) {
	assert (population.length() == agents);
	int[] kinds = new int[population.length()];
	for (int i = 0; i < population.length(); ++i) {
	    char c = population.charAt(i);
	    // find in existing string
	    boolean found = false;
	    for (int j = 0; j < KIND_CHARS.length(); ++j) {
		char c2 = KIND_CHARS.charAt(j);
		if (c == c2) {
		    found = true;
		    kinds[i] = j;
		    break;
		}
	    }
	    assert found; // otherwise unknown kind of population member
	}
	return createGame(kinds);
    }

    /** Creates a list of games */
    public List<Game> create() {
	it.reset();
	ArrayList<Game> result = new ArrayList<Game>();
	if (checker != null) {
	    it.setChecker(checker); // only "sorted" populations to avoid overcounting boring permutations
	}
	for (; !it.isStartedOver(); it.inc()) {
	    Game game = createGame(it.getDigits());
	    result.add(game);
	}
	return result;
    }

    /** Creates next game. Returns null if done. */
    public Game createNext() {
	if (it.isStartedOver()) {
	    it.inc();
	    return null;
	}
	Game game = createGame(it.getDigits());
	it.inc();
	return game;
    }

    /** Returns character representing population member with no or little motivaiton for either goal. */
    public char getDefaultChar() {
	return KIND_CHARS.charAt(DD);
    }

    private void setAgents(int _agents) {
	agents = _agents;
    }



}