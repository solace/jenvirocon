package org.planetsolace.jenvirocon;

import java.util.*;
import java.io.*;

class CoordinationGameFactoryAllTest {

    static int verbose = 1;

    static void run(int agents) {
	double[][] thresholds = new double[2][2];
	double threshVariation = 1.0;
	double threshPerGoal = agents / (2.0); // total commitment divided by goals
	thresholds[0][0] = threshPerGoal - threshVariation;
	thresholds[0][1] = threshPerGoal + threshVariation;
	thresholds[1][0] = threshPerGoal + threshVariation;
	thresholds[1][1] = threshPerGoal - threshVariation;
	CoordinationGameFactoryAll factory = new CoordinationGameFactoryAll(agents);
	Game game;
	GameScorer scorer = new AverageGoalScorer();
	List<GameScorer> scorers = new ArrayList<GameScorer>();
	scorers.add(scorer);
	scorers.add(new AllGoalScorer());
	scorers.add(new BlackSheepScorer(factory));
	scorers.add(new VariableLoadScorer(thresholds));
	scorers.add(new SpectrumScorer());
	scorers.add(new BiasScorer());
	scorers.add(new CentristScorer(0.25));
	System.out.print("\"Agents\" \"Goals\" \"Motivations\" \"MotivationSum\" \"MeanMotivation\" \"Variance\" \"Polarization\"");
	for (int j = 0; j < scorers.size(); ++j) { // different scores
	    System.out.print(" \"" + scorers.get(j).getName() + "\"");
	}
	System.out.println();

	while ((game = factory.createNext()) != null) {
	    int goals = game.getGoalCount();
	    double motSum = game.computeMotivationSum();
	    double motSqSum = game.computeMotivationSquareSum();
	    double ex = motSum / (agents * goals);
	    double ex2 = motSqSum / (agents * goals);
	    double variance = ex2 - (ex * ex); // simple variance over motivation matrix
	    double polarization = game.computeMeanPolarization();
	    System.out.print("" + agents + " " + goals + " " + game.getName() + " " + motSum + " " + (motSum/(agents * goals)) + " " + variance + " " + polarization);
	    for (int j = 0; j < scorers.size(); ++j) { // different scores
		System.out.print(" " + scorers.get(j).computeScore(game));
	    }
	    System.out.println();
	    assert game.getGoalCount() == 2;
	    if (verbose > 1) {
		System.out.println("#### DETAILS #####");
		System.out.println(game.toString());
		List<ArrayList<Integer> > equilibria = game.findNashEquilibria();
		game.printEquilibria(System.out, equilibria);
	    }
	}
    }

    public static void main(String[] args) {
	int agents = 4;
	int goalCount = 2;
	if (args.length > 0) {
	    try {
		agents = Integer.parseInt(args[0]);
	    } catch (NumberFormatException nfe) {
		System.out.println("Error: cannot parse command line paramenter as integer value!");
		System.exit(1);
	    }
	}
	run(agents);
    }

}