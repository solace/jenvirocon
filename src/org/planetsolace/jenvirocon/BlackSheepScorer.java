package org.planetsolace.jenvirocon;

import java.util.*;

class BlackSheepScorer implements GameScorer {

    AverageGoalScorer scorer = new AverageGoalScorer();
    GameFactory factory;

    double defaultMotivation = 0.25;

    public BlackSheepScorer(GameFactory _factory) {
	factory = _factory;
    }

    public double computeScore(Game game) {
	// int goalCount = game.getGoalCount();
	int playerCount = game.getPlayerCount();
	// int[] choices = new int[playerCount];
	// double[][] motivations = game.getMotivations();
	// double[] mOrig = new double[goalCount];
	double scoreSum = 0.0;

	String name = game.getName(); // should be population descriptor like cH
	assert name.length() == playerCount;
	char d = factory.getDefaultChar();
	for (int i = 0; i < playerCount; ++i) {
	    String newName = new String(name);
	    newName = name.substring(0, i) + d + name.substring(i+1);
	    assert newName.length() == playerCount;
// 	    for (int j = 0; j < goalCount; ++j) {
// 		mOrig[j] = motivations[i][j];
// 		motivations[i][j] = defaultMotivation;
// 	    }
	    Game modGame = factory.createGame(newName);
	    // Game modGame = new Game(playerCount, motivations, game.getChoiceNames(), game.getCosts());
	    double score = scorer.computeScore(modGame);
	    // System.out.println("DD sub-Score " + (i+1) + " : " 
	    // + score);
	    scoreSum += score;
// 	    for (int j = 0; j < goalCount; ++j) {
// 		motivations[i][j] = mOrig[j]; // restore to original value=
// 	    }	    
	}
	scoreSum /= playerCount;
	return scoreSum;
    }

    public String getName() { return new String("DD"); }

}