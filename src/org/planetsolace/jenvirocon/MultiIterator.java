package org.planetsolace.jenvirocon;

import java.util.Random;

class MultiIterator {

    int[] digits;

    int[] bases;

    DigitChecker checker;

    boolean startedOver = false;

    Random rnd = new Random();
    
    public MultiIterator(int[] _bases) {
	assert _bases != null;
	bases = new int[_bases.length];
	for (int i = 0; i < _bases.length; ++i) {
	    bases[i] = _bases[i];
	}
	digits = new int[bases.length];
    }

    int[] getBases() { return bases; }
    
    int[] getDigits() { return digits; }

    /** Returns randomized digits without changing the status of the iterator. */
    int[] createRandom() {
	int[] result = new int[digits.length];
	do {
	    for (int i = 0; i < result.length; ++i) {
		result[i] = rnd.nextInt(bases[i]);
	    }
	} while ((checker != null) && (!checker.isValid(result)));
	return result;
    }

    boolean hasNext() {
	for (int i = 0; i < digits.length; ++i) {
	    if (digits[i]+1 < bases[i]) {
		return true;
	    }
	}
	return false;
    }

    boolean inc() {
	startedOver = false;
	do {
	    boolean result = true;
	    for (int i = 0; i < digits.length; ++i) {
		digits[i] = digits[i] + 1;
		if (digits[i] >= bases[i]) {
		    digits[i] = 0;
		    if (i+1 == digits.length) {
			startedOver = true;
			result = false;
			break;
		    }
		} else {
		    break;
		}
	}
	} while ((checker != null) && (!checker.isValid(digits)));
	return true;
    }

    public boolean isStartedOver() { return startedOver; }

    public void reset() {
	for (int i = 0; i < digits.length; ++i) {
	    digits[i] = 0;
	}
    }


    public void setChecker(DigitChecker _checker) { checker = _checker; }
    
    static void test1() {
	int[] bases = new int[3];
	bases[0] = 3;
	bases[1] = 3;
	bases[2] = 3;
	System.out.println("Testing MultiIterator with 3 3 3:");
	for (MultiIterator it = new MultiIterator(bases); !it.isStartedOver(); it.inc()) {
	    GameTools.printArray(System.out, it.getDigits());
	    System.out.println();
	}
	System.out.println("Should be 27 cases from 0 0 0 to 2 2 2!");
    }

    static void test2Base2() {
	int[] bases = new int[2];
	bases[0] = 2;
	bases[1] = 2;
	System.out.println("Testing MultiIterator with 2 2:");
	for (MultiIterator it = new MultiIterator(bases); !it.isStartedOver(); it.inc()) {
	    GameTools.printArray(System.out, it.getDigits());
	    System.out.println();
	}
	System.out.println("Should be 4 cases from 0 0 to 1 1!");
    }

}