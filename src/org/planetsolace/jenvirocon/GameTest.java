package org.planetsolace.jenvirocon;

import java.util.*;

class GameTest {

    static void testPrisonersDilemma() {
	int agents = 2;
	String[] choices = {"C", "D"};
	int goalCount = 1;
	double[] costs = {-1, 0};
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 1.0;
	motivations[1][0] = 1.0;
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs);
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
    }

    static void testStagHunt() {
	System.out.println("Starting testStagHunt!");
	int agents = 2;
	String[] choices = {"C", "D"};
	int goalCount = 1;
	double[] costs = {1, 0};
	double[] iqaversions = new double[agents];
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 1.5;
	motivations[1][0] = 1.5;
	Game game = new Game(agents, iqaversions, motivations, choices, costs);
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	assert nash.size() == 2; // must be 2 Nash equilibria of mutual defection and mutual cooperation
	System.out.println("Finished testStagHunt!");
    }

    static void testDoubleStagHunt() {
	int agents = 2;
	String[] choices = {"C", "D"};
	int goalCount = 2;
	double[] costs = {1, 0};
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 1.5; // first player is motivated to solve goal 1
	motivations[1][0] = 1.5; // second player is motivated to solve goal 1
	motivations[0][1] = 1.5; // second player is motivated to solve goal 2
	motivations[1][1] = 1.5; // second player is motivated to solve goal 2
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs);
	game.testConvertOverallToGoalChoices();
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	// assert nash.size() == 4; // must be 2 Nash equilibria of mutual defection and mutual cooperation	
    }

    static void testDoubleStagHunt2() {
	int agents = 2;
	String[] choices = {"H", "C", "D"};
	int goalCount = 2;
	double[] costs = {2, 1, 0};
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 2.5; // first player is motivated to solve goal 1
	motivations[1][0] = 0.5; // second player is motivated to solve goal 1
	motivations[0][1] = 0.5; // second player is motivated to solve goal 2
	motivations[1][1] = 2.5; // second player is motivated to solve goal 2
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs);
	game.testConvertOverallToGoalChoices();
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	// assert nash.size() == 4; // must be 2 Nash equilibria of mutual defection and mutual cooperation	
    }

    static void testDoubleStagHunt2B() {
	int agents = 2;
	String[] choices = {"H", "C", "D"};

	int goalCount = 2;
	double[] costs = {1, 0.5, 0};
	double[][] motivations = new double[agents][goalCount];
	double[] thresholds = {1.0, 1.0};
	motivations[0][0] = 1.1; // first player is motivated to solve goal 1
	motivations[1][0] = 0.1; // second player is motivated to solve goal 1
	motivations[0][1] = 0.1; // second player is motivated to solve goal 2
	motivations[1][1] = 1.1; // second player is motivated to solve goal 2
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs, thresholds);
	game.testConvertOverallToGoalChoices();
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	// assert nash.size() == 4; // must be 2 Nash equilibria of mutual defection and mutual cooperation	
    }

    static void testDoubleStagHunt2Goal4PlayersHDHDDHDH() {
	int agents = 4;
	String[] choices = {"H", "C", "D"};
	int goalCount = 2;
	double[] costs = {2, 1, 0};
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 2.5; // first player is motivated to solve goal 1
	motivations[1][0] = 2.5; // second player is motivated to solve goal 1
	motivations[2][0] = 0.5; // second player is motivated to solve goal 1
	motivations[3][0] = 0.5; // second player is motivated to solve goal 1
	motivations[0][1] = 0.5; // second player is motivated to solve goal 2
	motivations[1][1] = 0.5; // second player is motivated to solve goal 2
	motivations[2][1] = 2.5; // second player is motivated to solve goal 2
	motivations[3][1] = 2.5; // second player is motivated to solve goal 2
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs);
	game.testConvertOverallToGoalChoices();
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	// assert nash.size() == 4; // must be 2 Nash equilibria of mutual defection and mutual cooperation	
    }

    static void testDoubleStagHunt2Goal4PlayersCCCCCCCC() {
	int agents = 4;
	String[] choices = {"H", "C", "D"};
	int goalCount = 2;
	double[] costs = {2, 1, 0};
	double[][] motivations = new double[agents][goalCount];
	motivations[0][0] = 1.5; // first player is motivated to solve goal 1
	motivations[1][0] = 1.5; // second player is motivated to solve goal 1
	motivations[2][0] = 1.5; // second player is motivated to solve goal 1
	motivations[3][0] = 1.5; // second player is motivated to solve goal 1
	motivations[0][1] = 1.5; // second player is motivated to solve goal 2
	motivations[1][1] = 1.5; // second player is motivated to solve goal 2
	motivations[2][1] = 1.5; // second player is motivated to solve goal 2
	motivations[3][1] = 1.5; // second player is motivated to solve goal 2
	double[] aversions = new double[agents];
	Game game = new Game(agents, aversions, motivations, choices, costs);
	game.testConvertOverallToGoalChoices();
	MultiMatrix payoffs = game.getPayoffs();
	System.out.println("Payoffs:");
	System.out.println(payoffs.toString());
	ArrayList<ArrayList<Integer> > nash = game.findNashEquilibria();	
	game.printEquilibria(System.out, nash);
	// assert nash.size() == 4; // must be 2 Nash equilibria of mutual defection and mutual cooperation	
    }

    public static void main(String[] args) {
	System.out.println("Starting GameTest.main!");
	MultiIterator.test1();
	MultiIterator.test2Base2();
	// testPrisonersDilemma();
	// testStagHunt();
	// testDoubleStagHunt2();
	testDoubleStagHunt2B();
	// testDoubleStagHunt2Goal4Players();
	// testDoubleStagHunt2Goal4PlayersHDHDDHDH();
	// testDoubleStagHunt2Goal4PlayersCCCCCCCC();
    }

}