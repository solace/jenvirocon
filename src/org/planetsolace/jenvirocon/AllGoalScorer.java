package org.planetsolace.jenvirocon;

import java.util.*;

class AllGoalScorer implements GameScorer {

    public double computeScore(Game game) {
	int goalCount = game.getGoalCount();
	int playerCount = game.getPlayerCount();
	int[] choices = new int[playerCount];
	List<ArrayList<Integer> > equilibria = game.findNashEquilibria();
	double goalSum = 0.0;
	for (int i = 0; i < equilibria.size(); ++i) {
	    for (int j = 0; j < equilibria.get(i).size(); ++j) {
		choices[j] = equilibria.get(i).get(j);
	    }
	    boolean[] achieved = game.goalsAchieved(choices);
	    boolean missedGoals = false;
	    for (int j = 0; j < achieved.length; ++j) {
		if (!achieved[j]) {
		    missedGoals = true;
		    break;
		}
	    }
	    if (!missedGoals) {
		goalSum += 1.0;
	    }
	}
	goalSum /= equilibria.size(); // average
	return goalSum;
    }

    public String getName() { return new String("ALL"); }

}