package org.planetsolace.jenvirocon;

/** Returns true, if and only if all digits are sorted */
class SortedChecker implements DigitChecker {

    /** Returns true, if and only if all digits are sorted */
    public boolean isValid(int[] digits) {
	for (int i = 1; i < digits.length; ++i) {
	    if (digits[i-1] > digits[i]) {
		return false;
	    }
	}
	return true;
    }

}