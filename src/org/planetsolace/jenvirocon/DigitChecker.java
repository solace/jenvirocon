package org.planetsolace.jenvirocon;

interface DigitChecker {

    boolean isValid(int[] digits);

}