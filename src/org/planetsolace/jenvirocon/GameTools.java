package org.planetsolace.jenvirocon;

import java.io.*;
import java.util.*;

class GameTools {

    public static void printArray(PrintStream ps, int[] digits) {
	for (int i = 0; i < digits.length; ++i) {
	    ps.print("" + digits[i] + " ");
	}
    }

    public static void printArray(PrintStream ps, double[] digits) {
	for (int i = 0; i < digits.length; ++i) {
	    ps.print("" + digits[i] + " ");
	}
    }

}