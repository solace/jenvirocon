package org.planetsolace.jenvirocon;

interface GameScorer {

    double computeScore(Game game);

    String getName();

}