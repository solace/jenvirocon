package org.planetsolace.jenvirocon;

import java.util.*;

/** Computes difference in overall motivation between highest-motivation and lowest-motivation goal */
class BiasScorer implements GameScorer {

    /** Computes score of a game. In this case it scores the motivations, not the outcome of the game. */
    public double computeScore(Game game) {
	int goalCount = game.getGoalCount();
	if (goalCount < 2) {
	    return 0.0;
	}
	int playerCount = game.getPlayerCount();
	double[][] motivations = game.getMotivations();
	double minMotivation = 1e20;
	double maxMotivation = -1e20;
	for (int i = 0; i < goalCount; ++i) {
	    double goalSum = 0.0;
	    for (int j = 0; j < playerCount; ++j) {
		goalSum += motivations[j][i];
	    }
	    if (goalSum > maxMotivation) {
		maxMotivation = goalSum;
	    }
	    if (goalSum < minMotivation) {
		minMotivation = goalSum;
	    }
	}
	return maxMotivation - minMotivation;
    }

    public String getName() { return new String("Bias"); }

}