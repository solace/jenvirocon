package org.planetsolace.jenvirocon;

import java.util.*;
import java.io.*;

class Game {

    /** Number of choices per player per goal */
    int choiceCount;

    int overallChoiceCount;

    int goalCount;

    int playerCount;

    /** Names of choices per goal (like cooperate, defect, heroic, or C, D, H. The length is equal to choiceCount. */
    String[] choiceNames;

    /** For each goal, the costs (typically negative utility) of making a certain choices. Length if equal to choiceCount. */
    double[] costs; 

    double[] goalThresholds;

    double[] inequalityAversions; // for each player, multiply standard deviation of payments with this number to payoff

    /** For each player and each goal, the motivations (difference in utility) to reach each of the goalCount goals. Length is equal to goalCount */
    double[][] motivations;

    /** A game can be given a "name" for future reference */
    String name = "game";

    /** Payoff matrix. Number of dimensionsi is equal to number of players. Each player has goalCount to the power of choiceCount overall choices */
    MultiMatrix payoffs;
    
    int verbose = 0;

    public Game(int _playerCount, double[] _inequalityAversions, double[][] _motivations, String[] _choiceNames, double[] _costs)  {
	playerCount = _playerCount;
	motivations = _motivations;
	choiceNames = _choiceNames;
	costs = _costs;
	inequalityAversions = _inequalityAversions;
	choiceCount = choiceNames.length;
	goalCount = motivations[0].length;
	initializePayoffMatrix();
    }

    public Game(int _playerCount, double[] _inequalityAversions, double[][] _motivations, String[] _choiceNames, double[] _costs, double[] _goalThresholds)  {
	playerCount = _playerCount;
	motivations = _motivations;
	choiceNames = _choiceNames;
	costs = _costs;
	inequalityAversions = _inequalityAversions;
	choiceCount = choiceNames.length;
	goalCount = motivations[0].length;
	goalThresholds = _goalThresholds;
	initializePayoffMatrix();
    }

    void initializePayoffMatrix() {
    	int overallChoices = 1;
	// integer version of Math.pow(goalCount, choiceCount); // overall choices per player
	for (int i = 0; i < goalCount; ++i) {
	    overallChoices *= choiceCount;
	}
	overallChoiceCount = overallChoices;
	int[] choices = new int[playerCount];
	for (int i = 0; i < choices.length; ++i) {
	    choices[i] = overallChoices;
	}
	int[] dims = new int[playerCount+1];
	dims[0] = playerCount;
	for (int i = 1; i < dims.length; ++i) {
	    dims[i] = overallChoices;
	}
	if (goalThresholds == null) {
	    goalThresholds = new double[goalCount];
	    for (int i = 0; i < goalCount; ++i) {
		goalThresholds[i] = (double)playerCount; // default thresholds
	    }
        }
	payoffs = new MultiMatrix(dims);
	for (MultiIterator it = new MultiIterator(choices); !it.isStartedOver(); it.inc()) {
	    int[] _choices = it.getDigits();
	    double[] payoff = computePayoffs(_choices);
	    assert payoff.length == playerCount;
	    for (int i = 0; i < payoff.length; ++i) {
		payoffs.setValue(i, _choices, payoff[i]);
	    }
	} 
	// iterator overall all choices of all players (iterate over all playerCount x choiceCount matrixces
    }

    /** One player has goalCount to the power of choiceCount choices. */
    private int[] convertOverallToGoalChoices(int choice) {
	if (verbose > 2) {
	    System.out.println("Starting convertOverallToGoalChoices : " + choice + " choices per goal: " + choiceCount);
	}
	int[] result = new int[goalCount];
	for (int i = 0; i < result.length; ++i) {
	    result[i] = choice % choiceCount;
	    if (choice < choiceCount) {
		break;
	    }
	    choice = choice / choiceCount;
	}
	if (verbose > 2) {
	    System.out.println("Finished convertOverallToGoalChoices : ");
	    GameTools.printArray(System.out, result);
	    System.out.println();
	}
	return result;
    }

    /** One player has goalCount to the power of choiceCount choices. */
    private int[][] convertOverallToGoalChoices(int[] choices) {
	int[][] result = new int[choices.length][goalCount];
	for (int i = 0; i < choices.length; ++i) {
	    int[] goalChoices = convertOverallToGoalChoices(choices[i]);
	    assert(goalChoices.length == result[0].length);
	    for (int j = 0; j < goalChoices.length; ++j) {
		result[i][j] = goalChoices[j];
	    }
	}
	return result;
    }

    /** One player has goalCount to the power of choiceCount choices. */
    void testConvertOverallToGoalChoices() {
	if (verbose > 2) {
	    System.out.println("Starting testConvertOverallToGoalChoices with choices per goal: " + choiceCount);
	}
	for (int i = 0; i < overallChoiceCount; ++i) {
	    int[] goalChoices = convertOverallToGoalChoices(i);
	    if (verbose > 2) {
		System.out.println("Overall choice: " + i);
		System.out.println("Goal choices:");
		GameTools.printArray(System.out, goalChoices);
		System.out.println();
	    }
	}
	if (verbose > 2) {
	    System.out.println("Finished testConvertOverallToGoalChoices with choices per goal: " + choiceCount);
	}
    }

    /** One player has goalCount to the power of choiceCount choices. */
    private int convertGoalChoiceToOverallChoices(int[] choices) {
	int result = 0;
	int mul = 1;
	for (int i = 0; i < choices.length; ++i) {
	    result += mul * choices[i];
	    mul *= choiceCount;
	}
	return result;
    }

    /** Computes payoffs for each of the players (first dimension), given their choices for each of the goals (second dimension). Returns array of double with length equal to number of players */
    double[] computePayoffs(int[] overallChoices) {
	assert overallChoices.length == playerCount;
	// compute costs paid for each goal:
	double[] goalPayments = new double[goalCount]; // payments for each goal
	double[] playerCosts = new double[playerCount];
	if (verbose > 2) {
	    System.out.println("Computing payoffs:");
	    GameTools.printArray(System.out, overallChoices);
	}
	for (int i = 0; i < playerCount; ++i) {
	    int[] goalChoices = convertOverallToGoalChoices(overallChoices[i]);
	    if (verbose > 2) {
		System.out.println("Agent " + (i+1) + " overall choice id:" + overallChoices[i] + " Goal Choices:");
		GameTools.printArray(System.out, goalChoices);
	    }
	    for (int j = 0; j < goalChoices.length; ++j) {		
		goalPayments[j] += costs[goalChoices[j]];
		playerCosts[i] += costs[goalChoices[j]];
		if (verbose > 2) {
		    System.out.println("Payment of agent " + (i+1) + " to goal " + (j+1) + " : " + costs[goalChoices[j]]);
		}
	    }
	    if (verbose > 2) {
		System.out.println("Goal Payments:");
		GameTools.printArray(System.out, goalPayments);
	    }
	}
	boolean[] goalAchieved = new boolean[goalCount];
	for (int i = 0; i < goalCount; ++i) {
	    goalAchieved[i] = (goalPayments[i] >= goalThresholds[i]);
	}
	double[] payoffv = new double[playerCount];
	double playerCostSum = 0.0;
	double playerCostSumSq = 0.0;
	for (int i = 0; i < playerCount; ++i) {
	    playerCostSum += playerCosts[i];
	    playerCostSumSq += playerCosts[i] * playerCosts[i];
	    for (int j = 0; j < goalCount; ++j) {
		if (goalAchieved[j]) {
		    payoffv[i] += motivations[i][j];
		}
	    }
	    payoffv[i] -= playerCosts[i]; // reduce by costs!
	}
	double playerCostMean = playerCostSum / playerCount; // cost (payment) towards goals averaged over all players
	double playerCostVariance = playerCostSumSq/playerCount - playerCostMean * playerCostMean; // formula for variance E(X^2)-(E(X))^2
	if (playerCostVariance < 0.0) { // could happen due to rounding errors
	    playerCostVariance = 0.0;
	}
	double playerCostSd = Math.sqrt(playerCostVariance);
	for (int i = 0; i < playerCount; ++i) {
	    payoffv[i] -= inequalityAversions[i] * playerCostSd;
	}
	return payoffv;
    }

    public String[] getChoiceNames() { return choiceNames; }

    public double[] getCosts() { return costs; }

    public int getGoalCount() { return goalCount; }

    public double[][] getMotivations() { return motivations; }

    /** Returns sum of all motivations */
    public double computeMotivationSum() { 
	double sum = 0.0;
	for (int i = 0; i < motivations.length; ++i) {
	    for (int j = 0; j < motivations[i].length; ++j) {
		sum += motivations[i][j];
	    }
	}
	return sum;
    }

    /** Returns sum of all squares of a motivations */
    public double computeMotivationSquareSum() { 
	double sum = 0.0;
	for (int i = 0; i < motivations.length; ++i) {
	    for (int j = 0; j < motivations[i].length; ++j) {
		sum += (motivations[i][j]*motivations[i][j]);
	    }
	}
	return sum;
    }

    /** Returns sum of all squares of a motivations */
    public double computeMeanPolarization() { 
	double sum = 0.0;
	for (int i = 0; i < motivations.length; ++i) { // loop over agents
	    double sum2 = 0.0;
	    double sumsq2 = 0.0;
	    for (int j = 0; j < motivations[i].length; ++j) { // loop over goals
		sum2 += motivations[i][j];
		sumsq2 += (motivations[i][j]*motivations[i][j]);
	    }
	    // E(X^2) - (E(X))^2
	    double avg2 = sum2/goalCount; // mean motivation for agent i
	    double variance = (sumsq2 / goalCount) - (avg2*avg2); // goal population variance (polarization) for agent i
	    sum += variance;
	}
	return sum / playerCount; // population variance of motivations averaged over players
    }

    public double[] getInequalityAversions() { return inequalityAversions; }

    public void setMotivations(double[][] _motivations) { motivations = _motivations; }

    public String getName() { return name; }

    public MultiMatrix getPayoffs() { return payoffs; }

    public int getPlayerCount() { return playerCount; }

    /** Returns true, iff choice combination corresponds to true pure Nash equilibrium */
    boolean isUnilateralOptimal(int[] choices, int agent) {
	double payOrig = payoffs.getValue(agent, choices);
	int choiceOrig = choices[agent];
	boolean weak = false;
	double[] payv = new double[overallChoiceCount];
	for (int i = 0; i < overallChoiceCount; ++i) {
	    if (i == choiceOrig) {
		payv[i] = payOrig;
		continue;
	    }
	    choices[agent] = i;
	    double pay = payoffs.getValue(agent, choices);
	    payv[i] = pay;
	    if (pay > payOrig) {
		choices[agent] = choiceOrig; // undo choice, overwise would lead to side effects of method
		return false;
	    }
	    if (pay == payOrig) {
		weak = true;
	    }
	}
	choices[agent] = choiceOrig; // avoid side effects
	if (verbose > 2) {
	    if (weak) {
		System.out.print("Weak");
	    } else {
		System.out.print("Strict");
	    }
	    System.out.println(" Nash equilibrium encountered for agent " + (agent+1) + " and encoded choice " + choiceOrig + "! Payoffs for that player and all available choices:");
	    GameTools.printArray(System.out, payv);
	    System.out.println("Original choices of all agents:");
	    GameTools.printArray(System.out, choices);
// 	    System.out.print(" : ");
// 	    for (int i = 0; i < choices.length; ++i) {
// 		System.out.print(choiceNames[choices[i]] + " ");
// 	    }
	    System.out.println();
	}
	return true;
    }

    /** Returns true, iff choice combination corresponds to true pure Nash equilibrium */
    boolean isNash(int[] choices) {
	for (int i = 0; i < playerCount; ++i) {
	    if (verbose > 2) {
		System.out.println("Testing isNash agent " + (i+1));
	    }
	    if (!isUnilateralOptimal(choices, i)) {
		return false;
	    }
	}
	return true;
    }

    /** Returns which goals are reached for a given overall agent choices */
    boolean[] goalsAchieved(int[] choices) {
	return goalsAchieved(convertOverallToGoalChoices(choices));
    }

    /** Returns which goals are reached for a given choice combination */
    boolean[] goalsAchieved(int[][] choices) {
	assert choices.length == playerCount;
	// compute costs paid for each goal:
	double[] goalPayments = new double[goalCount];
	for (int i = 0; i < playerCount; ++i) {
	    for (int j = 0; j < goalCount; ++j) {
		goalPayments[j] += costs[choices[i][j]];
	    }
	}
	//	System.out.print("Goal payments of this choice combo:");
	// for (int i = 0; i < goalPayments.length; ++i) {
	// System.out.print(" " + goalPayments[i]);
	// }
	// System.out.println();
	boolean[] goalAchieved = new boolean[goalCount];
	for (int i = 0; i < goalCount; ++i) {
	    goalAchieved[i] = (goalPayments[i] >= goalThresholds[i]);
	}
	return goalAchieved;
    }

    /** Returns array of choice combinations that correspond to Nash equilibria */
    public ArrayList<ArrayList<Integer> > findNashEquilibria() {
	int[] _choices = new int[playerCount];
	for (int i = 0; i < _choices.length; ++i) {
	    _choices[i] = overallChoiceCount;
	}
	if (verbose > 2) {
	    System.out.println("Starting findNashEquilibria with combination");
	    GameTools.printArray(System.out, _choices);
	    System.out.println();
	}
	ArrayList<ArrayList<Integer> > result = new ArrayList<ArrayList<Integer> >();
	MultiIterator it = new MultiIterator(_choices);
	while (!it.isStartedOver()) {
 	    int [] digits = it.getDigits();
	    if (verbose > 2) {
		System.out.println("Testing choice combination:");
		GameTools.printArray(System.out, digits);
		System.out.println();
	    }
 	    if (isNash(digits)){
		if (verbose > 2) {
		    System.out.println("NE found!");
		}
 		ArrayList<Integer> lst = new ArrayList<Integer>();
 		for (int i = 0; i < digits.length; ++i) {
 		    lst.add(digits[i]);
 		}
 		result.add(lst);
 	    } else {
		if (verbose > 2) {
		    System.out.println("No NE!");
		}
 	    }
	    it.inc();
	    if (verbose > 2) {
		System.out.println("Started over: " + it.isStartedOver());
	    }
	}
	if (verbose > 2) {
	    System.out.println("Finished findNashEquilibria!");
	}
	return result;
    }

    public void printEquilibria(PrintStream ps, List<ArrayList<Integer> > nash) {
	for (int i = 0; i < nash.size(); ++i) { // loop over equilibria
	    ps.println("Equilibrium " + (i+1) + " :");
	    for (int j = 0; j < nash.get(i).size(); ++j) { // loop over agents
		ps.print("Agent " + (j+1) + " : ");
		ps.print(" Choice " + (nash.get(i).get(j)+1) + " ");
		int[] goalChoices = convertOverallToGoalChoices(nash.get(i).get(j));
		for (int k = 0; k < goalChoices.length; ++k) {
		    ps.print("" + (goalChoices[k]+1) + "-" + choiceNames[goalChoices[k]] + " ");
		}
		ps.println();
	    }
	    ps.println("");
	}
    }

    public void setName(String _name) { name = new String(_name); }


    public String toString() {
	StringBuffer buf = new StringBuffer();
	/** A game can be given a "name" for future reference */
	buf.append("name: " + name + "\n");
	buf.append("choiceCount: " + choiceCount + "\n");
	buf.append("overallChoiceCount: " + overallChoiceCount + "\n");
	buf.append("goalCount: " + goalCount + "\n");
	buf.append("playerCount: " + playerCount + "\n");
	// buf.append("String[] choiceNames;

    /** For each goal, the costs (typically negative utility) of making a certain choices. Length if equal to choiceCount. */
	buf.append("costs:");
	for (int i = 0; i < costs.length; ++i) {
	    buf.append(" " +  costs[i]);
	}
	buf.append("\nthresholds:");
	for (int i = 0; i < goalThresholds.length; ++i) {
	    buf.append(" " +  goalThresholds[i]);
	}
    /** For each player and each goal, the motivations (difference in utility) to reach each of the goalCount goals. Length is equal to goalCount */
	buf.append("\nmotivations:\n");
	for (int i = 0; i < motivations.length; ++i) {
	    for (int j = 0; j < motivations[i].length; ++j) {
		buf.append(" " + motivations[i][j]);
	    }
	    buf.append("\n");
	}
	
	/** Payoff matrix. Number of dimensionsi is equal to number of players. Each player has goalCount to the power of choiceCount overall choices */
	// MultiMatrix payoffs;
	return buf.toString();
    }


}