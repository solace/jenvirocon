package org.planetsolace.jenvirocon;

import java.util.*;

/** Computes the fraction of centrists of a game. A centrist is defined as someone who cares about all goals approximately equally. This involves defining a threshold, what maximal difference in goal motivation is allowed. */
class CentristScorer implements GameScorer {

    double threshold;

    public CentristScorer(double _threshold) { threshold = _threshold; }

    /** Computes score of a game. In this case it scores the motivations, not the outcome of the game. */
    public double computeScore(Game game) {
	int goalCount = game.getGoalCount();
	if (goalCount < 2) {
	    return 0.0;
	}
	int playerCount = game.getPlayerCount();
	double[][] motivations = game.getMotivations();
	double distMax = 0.0;
	int centristCount = 0;
	for (int i = 0; i < playerCount; ++i) {
	    double motMin = 1e20;
	    double motMax = -1e20;
	    for (int j = 0; j < goalCount; ++j) {
		if (motivations[i][j] < motMin) {
		    motMin = motivations[i][j];
		}
		if (motivations[i][j] > motMax) {
		    motMax = motivations[i][j];
		}
	    }
	    double d = motMax - motMin; // maximum difference between goal motivation
	    if (d <= threshold) {
		++centristCount;
	    }
	}
	return centristCount / ((double)(playerCount)); // fraction of centrists
    }

    public String getName() { return new String("Center"); }

}