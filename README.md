# JEnviroCon #

This repository contains the Java sources corresponding to the manuscript:
Bindewald E: Achieving Multiple Goals via Voluntary Efforts and Motivation Asymmetry. arXiv:1503.05908 . <https://arxiv.org/abs/1503.05908>

### What is this repository for? ###

* This repository contains Java source code for performing a game-theoretic analysis of scenarios with different goals, different group sizes and different motivations of group members.
* Version 0.8.0

### How do I get set up? ###

* Dependencies: Java SDK for Java 6 and higher
* Compile: cd src; ./compile.sh
* Analyze groups for given number of goals and agents: ./coordinate.sh NUMBER_GOALS NUMBER_AGENTS


### Who do I talk to? ###

* Eckart Bindewald < ebindewald@frederick.edu>